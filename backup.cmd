
rem Set source and temporary directory paths
set "sourceDir1=C:\Users\user\Desktop\AVAL_keys"
set "sourceDir2=F:\ZVIT_ARC"
set "sourceDir3=F:\Банк Львів"
set "sourceDir4=C:\Users\user\Desktop"
set "tempDir=E:\backup_tmp"

rem Archive files and folders to temporary directory
if not exist E:\backup_tmp mkdir E:\backup_tmp
powershell Compress-Archive -Path "%sourceDir1%" -DestinationPath "%tempDir%\AVAL_keys_archive.zip"
powershell Compress-Archive -Path "%sourceDir2%" -DestinationPath "%tempDir%\ZVIT_ARC_archive2.zip"
powershell Compress-Archive -Path "%sourceDir3%" -DestinationPath "%tempDir%\Bank_Lviv_archive3.zip"
powershell Compress-Archive -Path "%sourceDir3%" -DestinationPath "%tempDir%\Desktop.zip"

rem Prepare FTP commands
(
    echo open 192.168.1.12
    echo user olsydor
    echo 123456
    echo binary
    echo cd /backup
    echo lcd "%tempDir%"
    echo put "AVAL_keys_archive.zip"
    echo put "ZVIT_ARC_archive2.zip"
    echo put "Bank_Lviv_archive3.zip"
    echo put "Desktop_archive3.zip"
    echo quit
) > ftpcmd.txt

rem Execute FTP commands
ftp -n -s:ftpcmd.txt

rem Clean up temporary files
del ftpcmd.txt